<?php

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('teste', function () {
    return 'ok';
});

Route::post('/cadastro', function (Request $request) {
    $data = $request->all();

    $validacao = Validator::make($data, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
    ]);

    if ($validacao->fails()) {
        return $validacao->errors();
    }

    $user = User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => bcrypt($data['password']),
    ]);


    $user->token = $user->createToken($user->email)->accessToken;

    return $user;
});

Route::post('/login', function (Request $request) {

    $validacao = Validator::make($request->all(), [
        'email' => 'required|string|email|max:255',
        'password' => 'required|string',
    ]);

    if ($validacao->fails()) {
        return $validacao->errors();
    }

    if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
        // Authentication passed...
        $user = Auth()->user();
        $user->token = $user->createToken($user->email)->accessToken;
        return $user;
    } else {
        return false;
    }
});

Route::middleware('auth:api')->get('/usuarios', function (Request $request) {
    return User::all();
});


Route::post('/products', function (Request $request) {
    $path = public_path('storage/');
    $file = base64_decode($request->img);
    file_put_contents($path . $request->filename, $file);

    $product = new App\Product;
    $product->name = $request->name;
    $product->img = $path  . 'imagem.png';
    $result = $product->save();

    if($result)
        return response()->json(['success' => true, 'product' => $product]);
    else
        return response()->json(['success' => false]);
});

Route::get('/products/{id}', function ($id) {
    $product = App\Product::find($id);

    return response()->json(['success' => true, 'imagem' => "storage/" . $product->name]);
});